#!/bin/sh

printf "Waiting for HDFS ..."
until hdfs dfs -ls /user/hive/warehouse ; do
	sleep 1
	printf "."
done
printf ". [OK]\n"

# This is done by the hadoop container.
# hdfs dfs -mkdir -p /tmp /user/hive/warehouse
# hdfs dfs -chmod g+w /tmp /user/hive/warehouse

if [ ! -d metastore_db ] ; then 
	mkdir -p /var/hive/metastore
	cd /var/hive/metastore
	printf "Creating Hive metastore....."
	schematool -initSchema -dbType derby > /tmp/hive-schema.log 2>&1
	if [ $? ] ; then
		printf "[OK]\n"
	else
		printf "[FAILED]\n"
		exit -1
	fi
fi
printf "Staring Hive ...\n"
cd /var/hive/metastore
hive --service hiveserver2 --hiveconf hive.server2.thrift.port=10000 \
	--hiveconf hive.root.logger=ERROR,console \
	--hiveconf hive.optimize.tez=true
