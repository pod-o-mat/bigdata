# HIVE

A Standalone Hive container

To run the container

```bash
docker run --name hive -ti -d -p 10000 hbouvier/hive
echo "Wait 30 seconds and run this:"
docker exec -ti hive beeline -u jdbc:hive2://localhost:10000/default "" "" ""
```

Within Hive use the dummy `name` table you have just created

```bash
# create the directory
hdfs dfs -mkdir names
# copy the text file with the data
hdfs dfs -put  samples/name/names.txt names/
# make sure `anonymous` can write into the directory to INSERT rows
hdfs dfs -chmod -R a+w /user/root/names
# start beehive
./bee
```


```sql
CREATE EXTERNAL TABLE name (id INT, name STRING) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/user/root/names/';
SHOW TABLES;
DESCRIBE name;
SELECT * FROM name;
INSERT INTO TABLE name VALUES (4, 'zeus'), (5, 'odin');
SELECT * FROM name;
SELECT * FROM name ORDER BY id;

```


# not sure about this
```hive
beeline> SET mapred.job.tracker=hadoop:50030;
beeline> SET -v;
```

or 
```hive
beeline> SET mapreduce.framework.name=local;

# for hivecli?
beeline> SET hive.exec.mode.local.auto=false;
```

# KNOWN ISSUES
- MapReduce stop working when we use the `CREATE EXTERNAL TABLE` using hdfs. You have to restart hive container (or kill the java process) to restart MapRedude.
