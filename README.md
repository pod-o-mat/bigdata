# Bigdata

To run a local version of Apache HADOOP on your computer with HBase, Hive and Spark. At this time only the "all-in-one" image is working properly. Here are the instructions to get it up and running.

## INITIAL SETUP
First you have to create the `hadoop` network. To create it on a docker swarm use:
```bash
$ docker network create -d overlay hadoop
```

On a standalone docker installation
```bash
$ docker network create hadoop
```

Then make sure that you cleanup previous data created by HADOOP hdfs, hbase and hive. This is also the `fix` when hadoop is not starting.

```bash
$ docker volume rm bigdata_hadoop_data bigdata_hadoop_hdfs bigdata_hadoop_hbase bigdata_hadoop_hive_metastore_db
```

## Startup hadoop

If you don't want the whole shebang, you can edit the `docker-compose-all-in-one.yaml` file and comment out the part of the stack you don't want to run in the `environment` section. Let's say you want to only run `Kafka` you can leave ONLY `- KAFKA=true` in the `environment:` section and only `Kafka` and it's dependency `Zookeeper` will be started in the image. Also, if you only want the hadoop stack and not Zeppelin nor Nifi, you can specify it on the command line like this `docker-compose -f docker-compose-all-in-one.yaml up -d hadoop` or if you want hadoop and nifi: `docker-compose -f docker-compose-all-in-one.yaml up -d hadoop nifi`.

```bash
$ docker-compose -f docker-compose-all-in-one.yaml up -d
```

It will take about 2 minutes for HIVE to become available, you can check the logs and wait until you see the two line with `Hive Session ID = XXX` one after the other.
```bash
$ docker logs -f hadoop

20/04/16 12:37:07 INFO namenode.NameNode: registered UNIX signal handlers for [TERM, HUP, INT]
.
.
.
2020-04-16 12:37:52,263 WARN  [main] DataNucleus.MetaData: Metadata has jdbc-type of null yet this is not valid. Ignored
2020-04-16 12:37:52,264 WARN  [main] DataNucleus.MetaData: Metadata has jdbc-type of null yet this is not valid. Ignored
2020-04-16 12:37:52,264 WARN  [main] DataNucleus.MetaData: Metadata has jdbc-type of null yet this is not valid. Ignored
2020-04-16 12:37:52,264 WARN  [main] DataNucleus.MetaData: Metadata has jdbc-type of null yet this is not valid. Ignored
2020-04-16 12:37:52,265 WARN  [main] DataNucleus.MetaData: Metadata has jdbc-type of null yet this is not valid. Ignored
Hive Session ID = 7a196c90-4acd-4520-ae24-f9092436a335
2020-04-16 12:38:38,777 INFO  [ReadOnlyZKClient-hadoop:2181@0x61378edf] zookeeper.ZooKeeper: Session: 0x1001762dad50004 closed
2020-04-16 12:38:38,777 INFO  [ReadOnlyZKClient-hadoop:2181@0x61378edf-EventThread] zookeeper.ClientCnxn: EventThread shut down for session: 0x1001762dad50004
2020-04-16 12:38:45,538 INFO  [ReadOnlyZKClient-hadoop:2181@0x38e9df98] zookeeper.ZooKeeper: Session: 0x1001762dad50003 closed
2020-04-16 12:38:45,539 INFO  [ReadOnlyZKClient-hadoop:2181@0x38e9df98-EventThread] zookeeper.ClientCnxn: EventThread shut down for session: 0x1001762dad50003
Hive Session ID = 26dc6ce8-c2e1-4a51-84fa-4eef78868a32
Hive Session ID = baf1b065-4172-4595-afa8-04cdf74e5086
  ^=== HIVE is ready to ROCKnROLL!
 ^C
docker run -ti hadoop hql
```

### Known issues

[X] Creating an Hive table on Hbase does not work.
[ ] HDFS permissions are not great... Sharing data between hadoop component requires manual tweeking of the permission through `hdfs` command line tool.

## HDFS
When permissions goes wrong...

```bash
docker exec -ti --user haddop hadoop /opt/hadoop/bin/hdfs dfs -chmod -R 777 / 
```

## Zeppelin

Open the [Zeppelin installation verification Notebook](http://localhost:9080/#/notebook/2F5K8RUZ4) and run all the paragraphs to test your installation of HDFS, Spark, HBase, Hive and Kafka!

**Restarting an interpreter when it is failing**

Click on the `anonymous` menu on the top right corner and then choose the `Interpreter` menu item. In the search field at the top type the name of the interpreter (e.g. `Spark`). Then click on the `RESTART` button at the top right, between the `edit` and the `remove` button.

## Nifi

Open [Nifi](http://localhost:9090/nifi/) and run all the processgroups


## Hive

To go directly in hive

```bash
docker run --rm -ti --network=hadoop registry.gitlab.com/pod-o-mat/bigdata/hadoop-20200425-0003 beeline -u jdbc:hive2://hadoop:10000/default
```

To start a bash shell to create some data in HDFS

```bash
docker run --rm -ti --network=bigdata_hadoop registry.gitlab.com/pod-o-mat/bigdata/hadoop-20200425-0003 bash

printf "1\tbob\n2\tjoe\n3\trichard\n" > names.txt
hdfs dfs -mkdir data
hdfs dfs -chmod -R o+w data
hdfs dfs -put names.txt data
hdfs dfs -ls data
hdfs dfs -cat data/names.tx
```

```bash
beeline -u jdbc:hive2://hadoop:10000/default

CREATE EXTERNAL TABLE name (id INT, name STRING) ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' LOCATION '/user/root/data/';
SHOW TABLES;
DESCRIBE name;
SELECT * FROM name;
INSERT INTO TABLE name VALUES (4, 'zeus'), (5, 'odin');
SELECT * FROM name;
SELECT * FROM name ORDER BY id;
```


# Using seperate docker images

This is not working at this time... Work in progres.!

### Troubleshooting

```
ERROR : Ended Job = job_local236735723_0002 with errors
ERROR : FAILED: Execution Error, return code 2 from org.apache.hadoop.hive.ql.exec.mr.MapRedTask
Error: Error while processing statement: FAILED: Execution Error, return code 2 from org.apache.hadoop.hive.ql.exec.mr.MapRedTask (state=08S01,code=2)
```
You have to restart the `hive` server :-(

```bash
docker-compose restart hive
```

Hive server restarted
```
Unknown HS2 problem when communicating with Thrift server.
Error: org.apache.thrift.transport.TTransportException: java.net.SocketException: Broken pipe (Write failed) (state=08S01,code=0)
```

you have to reconnect to hive
```bash
^D
beeline -u jdbc:hive2://hive:10000/default
```


## HBase

```bash
docker run --rm -ti --network=bigdata_hadoop registry.gitlab.com/pod-o-mat/bigdata/hbase:v1.3.6-0001 bash -c '\
        sed -e "s/ZOOKEEPER_PORT/2181/" -i.port.orig /opt/hbase/conf/hbase-site.xml && \
        sed -e "s/ZOOKEEPER_HOST/zookeeper/" -i.host.orig /opt/hbase/conf/hbase-site.xml && \
        /opt/hbase/bin/hbase shell'
```

```bash
status
create 'mytest', 'cf'
list 'mytest'
describe 'mytest'
put 'mytest', 'row1', 'cf:a', 'value1'
put 'mytest', 'row2', 'cf:a', 'value2'
put 'mytest', 'row3', 'cf:a', 'value3'
scan 'mytest'
get 'mytest', 'row1'
```

To clean up
```bash
disable 'mytest'
enable 'mytest'
disable 'mytest'
drop 'mytest'

```


### Troubleshooting

```
2020-04-13 10:50:12,994 ERROR [main] zookeeper.RecoverableZooKeeper: ZooKeeper exists failed after 4 attempts
2020-04-13 10:50:12,995 WARN  [main] zookeeper.ZKUtil: hconnection-0x14b0e1270x0, quorum=ZOOKEEPER_HOST:2181, baseZNode=/hbase Unable to set watcher on znode (/hbase/hbaseid)
```
You did not substitute the ZOOKEEPER value in the XML files

---

Local Hadoop web ui

[Hive](http://localhost:10002/)

[HBase](http://localhost:16010/)

[Hadoop](http://localhost:18088/)

[Spark](http://localhost:8080/)

[Zeppelin](http://localhost:9080/)

