# Apache AirFlow

The airflow sqlite database is in `/app/airflow.db` and to add your own dags you can mount /app/dags into the container.

```bash
docker run -tid -p 8080:8080 -v `pwd`/data:/app registry.gitlab.com/pod-o-mat/airflow:v1.10.9
```

