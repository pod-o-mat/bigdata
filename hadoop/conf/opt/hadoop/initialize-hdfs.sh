#!/bin/sh

echo "Formating HDFS"
hdfs namenode -format
echo "Formating HDFS ... [OK]"

# Wait until HDFS is ready and create the base structure
# with the right permission for everyone.
(
	echo "Waiting for HDFS ..."
	until hdfs dfs -mkdir -p /tmp /data /user/anonymous /user/root /user/hadoop /user/hive/warehouse ; do
		sleep 1
	done
	echo "Setting up HDFS directory structure"
	hdfs dfs -chmod -R 777 /tmp /data /user/anonymous
	hdfs dfs -chown -R root /user/root /user/hive
	hdfs dfs -chmod g+w /user/hive/warehouse
	touch /tmp/hadoop-hadoop/hdfs-formated
	chmod 664 /tmp/hadoop-hadoop/hdfs-formated
	printf "\n---------------------------------------\n\n\n\nHDFS is READY\n\n\n"
	echo "HDFS [OK]"
) &
