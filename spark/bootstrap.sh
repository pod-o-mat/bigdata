#!/bin/sh

# Wait for SSHD to start HDFS and YARN
(
	printf "Waiting for sshd ."
	until ssh -Ao 'StrictHostKeyChecking no' localhost -o 'BatchMode=yes' -o 'ConnectionAttempts=1' true ; do
		printf "."
		sleep 1
	done
	printf " [OK]\n"
    /opt/spark/sbin/start-all.sh
	printf "Starging spark\n"
) &

/usr/sbin/sshd -D