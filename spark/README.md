# Spark

To start the spark 
```bash
docker run --name spark --hostname=spark -tid \
	- p 4040:4040 -p 7077:7077 \
	-v $(pwd)/samples/:/app/samples registry.gitlab.com/pod-o-mat/bigdata/spark:latest
```

To run the sample code from the container
```bash
docker exec -ti --rm spark bash
cd samples
make
```

You can submit your jobs at `spark://spark:7077` from within docker or `spark://localhost:7077` from docker for desktop host
