#!/bin/sh

sudo sed -e "s/localhost/hadoop/" -i.orig /opt/hadoop/etc/hadoop/core-site.xml
sudo sed -e "s/localhost/hadoop/" -i.orig /opt/hbase/conf/hbase-site.xml
sudo sed -e "s/localhost/hadoop/" -i.orig /opt/hive/conf/hive-site.xml
sudo sed -e "s/localhost/hadoop/" -i.orig /opt/kafka/config/server.properties

/opt/bin/all-start.sh

sudo /bin/watchgod -- /usr/sbin/sshd -D