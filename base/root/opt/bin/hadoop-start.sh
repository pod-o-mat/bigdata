#!/bin/sh -e

# This can run before sshd is running
if [ ! -r /tmp/hadoop-hadoop/hdfs-formated ] ; then
	echo "[HDFS] Formating hdfs volumes..."
	/opt/bin/hdfs-init.sh
	echo "[HDFS] hdfs volumes ready"
else
	echo "[HDFS] Repairing hdfs volumes..."
	yes | hdfs namenode -recover -force
	echo "[HDFS] hdfs volumes ready"
fi

# Wait for SSHD to start HDFS and YARN
(
	echo "[HADOOP] Waiting for sshd ..."
	until ssh -Ao 'StrictHostKeyChecking no' localhost -o 'BatchMode=yes' -o 'ConnectionAttempts=1' true ; do
		sleep 1
	done
	echo "[HADOOP] sshd ready"
	echo "[HDFS] Starging hdfs"
	/opt/hadoop/sbin/start-dfs.sh &
	echo "[HDFS] Starging yarn"
	/opt/hadoop/sbin/start-yarn.sh &
) &
