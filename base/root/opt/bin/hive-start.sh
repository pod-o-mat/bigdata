#!/bin/sh

(
	echo "[HIVE] Waiting for HDFS ..."
	until hdfs dfs -ls /user/hive/warehouse ; do
		sleep 1
	done
	echo "[HIVE] HDFS ready..."

	if [ ! -d metastore_db ] ; then 
		mkdir -p /var/hive/metastore
		cd /var/hive/metastore
		echo "[HIVE] Creating Hive metastore....."
		schematool -initSchema -dbType derby > /tmp/hive-schema.log 2>&1
		if [ $? ] ; then
			echo "[HIVE] metastore created"
		else
			echo "[HIVE] *** ERROR *** FAILED TO CREATE METASTORE"
			exit -1
		fi
	fi
	echo "[HIVE] Starting hive"
	cd /var/hive/metastore
	hive --service hiveserver2 --hiveconf hive.server2.thrift.port=10000 \
		--hiveconf hive.root.logger=ERROR,console \
		--hiveconf hive.optimize.spark=true
		# --hiveconf hive.optimize.tez=true
		
) &
