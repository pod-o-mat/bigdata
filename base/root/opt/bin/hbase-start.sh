#!/bin/sh

(
	echo "[HBASE] Waiting for HDFS ..."
	until hdfs dfs -ls /tmp ; do
		sleep 1
	done
	echo "[HBASE] Starting master"
	/opt/hbase/bin/hbase master start &
	/opt/hbase/bin/hbase regionserver start
	# /opt/hbase/bin/start-hbase.sh
) &
