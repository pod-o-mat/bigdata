#!/bin/sh

# Wait for SSHD to start HDFS and YARN
(
	echo "[SPARK] Waiting for sshd ."
	until ssh -Ao 'StrictHostKeyChecking no' localhost -o 'BatchMode=yes' -o 'ConnectionAttempts=1' true ; do
		sleep 1
	done
	echo "[SPARK] sshd ready"
	echo "[SPARK] Starting spark"
	export SPARK_DIST_CLASSPATH=$(/opt/hadoop/bin/hadoop classpath)
	export SPARK_MASTER_WEBUI_PORT=8083
	env
    /opt/spark/sbin/start-all.sh
	# /opt/spark/sbin/start-master.sh
	# /opt/spark/sbin/start-slave.sh spark://hadoop:7077

) &
