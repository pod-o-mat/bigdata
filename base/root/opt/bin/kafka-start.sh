#!/bin/sh

(
	echo "[KAFKA] Waiting for Zookeeper to be ready"
	until echo srvr | nc -w 2 -q 2 localhost 2181 ; do
		sleep 1;
	done
	echo "[KAFKA] Zookeeper ready"

	echo "[KAFKA] Starting kafka"
	/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties
) &