#!/bin/sh
# sudo -H -i -u hadoop 
if [ ! -z "${ZOOKEEPER}" -o ! -z "${KAFKA}" -o ! -z "${HADOOP}" -o ! -z "${HBASE}" -o ! -z "${HIVE}" ]; then
  /opt/bin/zookeeper-start.sh
fi

if [ ! -z "${HADOOP}" -o ! -z "${HBASE}" -o ! -z "${HIVE}" ]; then
  /opt/bin/hadoop-start.sh
fi

if [ ! -z "${HBASE}" ]; then
  /opt/bin/hbase-start.sh
fi

if [ ! -z "${SPARK}" ]; then
  /opt/bin/spark-start.sh
fi

if [ ! -z "${HIVE}" ]; then
  /opt/bin/hive-start.sh
fi

if [ ! -z "${KAFKA}" ]; then
  /opt/bin/kafka-start.sh
fi
