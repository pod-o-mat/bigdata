#!/bin/sh

echo "[HDFS] namenode format"
hdfs namenode -format -force
echo "[HDFS] namenode format [OK]"

# Wait until HDFS is ready and create the base structure
# with the right permission for everyone.
(
	echo "[HDFS] Waiting for hdfs ..."
	until hdfs dfs -mkdir -p /tmp /data /user/anonymous /user/root /user/hadoop /user/hive/warehouse ; do
		sleep 1
	done
	echo "[HDFS] Setting up HDFS directory structure"
	hdfs dfs -chmod -R 777 /tmp /data /user/anonymous
	hdfs dfs -chown -R root /user/root /user/anonymous
	hdfs dfs -chmod g+w /user/hive/warehouse /user/hadoop
	touch /tmp/hadoop-hadoop/hdfs-formated
	chmod 664 /tmp/hadoop-hadoop/hdfs-formated
	printf "\n---------------------------------------\n\n\n\n[HDFS] READY\n\n\n"
) &
