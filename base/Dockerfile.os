FROM registry.gitlab.com/pod-o-mat/bigdata/hadoop:all-in-one-apache-VERSION_DATE AS base
FROM openjdk:8-jdk-stretch
ENV MIRROR=http://apache.mirror.iweb.ca \
    HADOOP_VERSION=2.10.0 \
    HIVE_VERSION=3.1.2 \
    SPARK_VERSION=2.4.5 \
    HBASE_VERSION=2.2.4 \
    KAFKA_VERSION=2.4.1 \
    SCALA_VERSION=2.11 \
    ZOOKEEPER_VERSION=3.5.7 \
    WATCHGOD_VERSION=2.0.1-9d60f6c

COPY --from=base /tmp/watchgod /bin/watchgod
COPY --from=base /tmp/hadoop-${HADOOP_VERSION} /opt/hadoop/
COPY --from=base /tmp/hbase-${HBASE_VERSION} /opt/hbase/
COPY --from=base /tmp/apache-hive-${HIVE_VERSION}-bin /opt/hive/
COPY --from=base /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION} /opt/kafka/
COPY --from=base /tmp/spark-${SPARK_VERSION}-bin-without-hadoop /opt/spark/
COPY --from=base /tmp/apache-zookeeper-${ZOOKEEPER_VERSION}-bin /opt/zookeeper/

ENV USER_HADOOP=hadoop \
    GROUP_HADOOP=hadoop \
    UID_HADOOP=1000 \
    GID_HADOOP=1000 \
    HADOOP_HOME=/opt/hadoop \
    HADOOP_MAPRED_HOME=/opt/hadoop \
    HIVE_HOME=/opt/hive \
    SPARK_HOME=/opt/spark \
    HBASE_HOME=/opt/hbase \
    KAFKA_HOME=/opt/kafka \
    ZOOKEEPER_HOME=/opt/zookeeper \
    JAVA_HOME=/usr/local/openjdk-8 \
    HADOOP_PATH=/opt/hadoop/bin:/opt/hbase/bin:/opt/hive/bin:/opt/spark/bin:/opt/kafka/bin:/opt/zookeeper/bin:/opt/bin \
    PATH=${PATH}:/opt/hadoop/bin:/opt/hbase/bin:/opt/hive/bin:/opt/spark/bin:/opt/kafka/bin:/opt/zookeeper/bin:/opt/bin

RUN apt-get -y update && \
	apt-get -y install ssh pdsh sudo vim apt-transport-https netcat && \
    echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list && \
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add && \
    apt-get update -y && \
    apt-get install -y sbt make && \
 	rm -rf /var/lib/apt/lists/*

# When we have python 3.7 in zeppelin, this could be de recipe to install python 3.7 with spark
# #############################################################################################
# RUN apt update -y && \
#     apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget libbz2-dev && \
#     cd /tmp/ && \
#     wget https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tar.xz && \
#     tar -xf Python-3.7.2.tar.xz && \
#     cd Python-3.7.2 && \
#     ./configure --enable-optimizations && \
#     make -j 1 && \
#     make install && \
#     pip3 install fbprophet pandas pyarrow

RUN apt-get update -y && \
    apt-get install -y python3 python3-pip && \
    pip3 install --upgrade pip && \
    pip3 install fbprophet==0.6 pandas==0.25.3 pyarrow==0.17.0 hdfs==2.5.8

RUN	echo 'hadoop ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    echo 'ssh' > /etc/pdsh/rcmd_default && \
    printf "    StrictHostKeyChecking no\n    UserKnownHostsFile=/dev/null\n" >> /etc/ssh/ssh_config

RUN groupadd -g ${GID_HADOOP} ${GROUP_HADOOP} && \
    useradd -u ${UID_HADOOP} -g ${GID_HADOOP} -s /bin/bash ${USER_HADOOP} && \
    usermod -L ${USER_HADOOP}

RUN mkdir -p /run/sshd \
             /root/.ssh /home/hadoop/.ssh && \
    echo "export JAVA_HOME=${JAVA_HOME}" >> /home/hadoop/.bashrc && \
    echo "export PATH=\${PATH}:${JAVA_HOME}/bin:${HADOOP_PATH}" >> /home/hadoop/.bashrc && \
    echo "export HADOOP_HOME=${HADOOP_HOME}" >> /home/hadoop/.bashrc && \
    echo "export HADOOP_MAPRED_HOME=${HADOOP_MAPRED_HOME}" >> /home/hadoop/.bashrc && \
    echo "export HIVE_HOME=${HIVE_HOME}" >> /home/hadoop/.bashrc && \
    echo "export SPARK_HOME=${SPARK_HOME}" >> /home/hadoop/.bashrc && \
    echo "export HBASE_HOME=${HBASE_HOME}" >> /home/hadoop/.bashrc && \
    echo "export KAFKA_HOME=${KAFKA_HOME}" >> /home/hadoop/.bashrc && \
    echo "export ZOOKEEPER_HOME=${ZOOKEEPER_HOME}" >> /home/hadoop/.bashrc && \
    echo "export SPARK_DIST_CLASSPATH=\$(/opt/hadoop/bin/hadoop classpath)" >> /home/hadoop/.bashrc && \
    cat /home/hadoop/.bashrc >> /home/hadoop/.profile && \
    ssh-keygen -t rsa -P '' -f /home/hadoop/.ssh/id_rsa && \
    cat /home/hadoop/.ssh/id_rsa.pub >> /home/hadoop/.ssh/authorized_keys && \
    chown -R ${USER_HADOOP}:${GROUP_HADOOP} /home/hadoop && \
    chmod a+x /home/hadoop/.profile /home/hadoop/.bashrc && \
    chmod 700 /home/hadoop/.ssh && \
    chmod 600 /home/hadoop/.ssh/*
