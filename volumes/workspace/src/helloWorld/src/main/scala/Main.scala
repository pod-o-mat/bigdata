object Main {
  val first: Int = 0

  def main(args: Array[String]): Unit = {
    val name = if (args.isEmpty) "world" else args(first)
    println(s"Hello ${name}!")
  }
}
