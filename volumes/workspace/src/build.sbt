import scala.sys.process._

name := "sbt-multi-project"
organization in ThisBuild := "com.ruggedcode"
scalaVersion in ThisBuild := "2.11.12"

// PROJECTS
///////////////////////////////////////////////////////////////////////////////

lazy val global = project
  .in(file("."))
  .settings(settings)
  // .disablePlugins(AssemblyPlugin)
  .aggregate(
    common,
    helloWorld,
    sparkWordCount,
    microservice
  )

lazy val common = project
  .settings(
    name := "common",
    settings,
    libraryDependencies ++= commonDependencies
  )
// .disablePlugins(AssemblyPlugin)

lazy val helloWorld = project
  .settings(
    name := "helloWorld",
    settings,
    libraryDependencies ++= commonDependencies ++ Seq(
      // dependencies.monocleCore,
      // dependencies.monocleMacro
    )
  )
  // .disablePlugins(AssemblyPlugin)
  .dependsOn(
    common
  )

lazy val sparkWordCount = project
  .settings(
    name := "sparkWordCount",
    settings,
    // assemblySettings,
    libraryDependencies ++= commonDependencies ++ Seq(
      dependencies.sparksql
    )
  )
  // .disablePlugins(AssemblyPlugin)
  .dependsOn(
    common
  )

lazy val microservice = project
  .settings(
    name := "microservice",
    description := "A REST API to create binary CloudEvents",
    // mainClass in Compile := Some("com.ruggedcode.platform.eventing.api.cloudevent.ToCloudEventApplication"),
    settings,
    libraryDependencies ++= commonDependencies ++ Seq(
      dependencies.springbootstarterweb,
      dependencies.springbootconfigprocessor
    )
  )
  // .disablePlugins(AssemblyPlugin)
  .dependsOn(
    common
  )

// DEPENDENCIES
///////////////////////////////////////////////////////////////////////////////
lazy val dependencies =
  new {
    // val logbackV        = "1.2.3"
    // val logstashV       = "4.11"
    // val scalaLoggingV   = "3.7.2"
    // val slf4jV          = "1.7.25"
    // val typesafeConfigV = "1.3.1"
    // val pureconfigV     = "0.8.0"
    // val monocleV        = "1.4.0"
    // val akkaV           = "2.5.6"
    // val scalatestV      = "3.0.4"
    // val scalacheckV     = "1.13.5"
    val sparksqlV                  = "2.4.5"
    val springbootstarterwebV      = "1.5.4.RELEASE"
    val springbootconfigprocessorV = "1.5.4.RELEASE"

    // val logback        = "ch.qos.logback"             % "logback-classic"          % logbackV
    // val logstash       = "net.logstash.logback"       % "logstash-logback-encoder" % logstashV
    // val scalaLogging   = "com.typesafe.scala-logging" %% "scala-logging"           % scalaLoggingV
    // val slf4j          = "org.slf4j"                  % "jcl-over-slf4j"           % slf4jV
    // val typesafeConfig = "com.typesafe"               % "config"                   % typesafeConfigV
    // val akka           = "com.typesafe.akka"          %% "akka-stream"             % akkaV
    // val monocleCore    = "com.github.julien-truffaut" %% "monocle-core"            % monocleV
    // val monocleMacro   = "com.github.julien-truffaut" %% "monocle-macro"           % monocleV
    // val pureconfig     = "com.github.pureconfig"      %% "pureconfig"              % pureconfigV
    // val scalatest      = "org.scalatest"              %% "scalatest"               % scalatestV
    // val scalacheck     = "org.scalacheck"             %% "scalacheck"              % scalacheckV
    val sparksql                  = "org.apache.spark"         %% "spark-sql"                          % sparksqlV
    val springbootstarterweb      = "org.springframework.boot" % "spring-boot-starter-web"             % springbootstarterwebV
    val springbootconfigprocessor = "org.springframework.boot" % "spring-boot-configuration-processor" % springbootconfigprocessorV
  }

lazy val commonDependencies = Seq(
  // dependencies.logback,
  // dependencies.logstash,
  // dependencies.scalaLogging,
  // dependencies.slf4j,
  // dependencies.typesafeConfig,
  // dependencies.akka,
  // dependencies.scalatest  % "test",
  // dependencies.scalacheck % "test"
)

// SETTINGS
///////////////////////////////////////////////////////////////////////////////

lazy val settings =
commonSettings ++
wartremoverSettings ++
scalafmtSettings

lazy val compilerOptions = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding",
  "utf8"
)

lazy val commonSettings = Seq(
  scalacOptions ++= compilerOptions
  // resolvers ++= Seq(
  //   "Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository",
  //   Resolver.sonatypeRepo("releases"),
  //   Resolver.sonatypeRepo("snapshots")
  // )
)

lazy val wartremoverSettings = Seq(
  wartremoverWarnings in (Compile, compile) ++= Warts.allBut(Wart.Throw, Wart.Null, Wart.PublicInference, Wart.Var)
)

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true,
    scalafmtTestOnCompile := true,
    scalafmtVersion := "1.2.0"
  )

// lazy val assemblySettings = Seq(
//   assemblyJarName in assembly := name.value + ".jar",
//   assemblyMergeStrategy in assembly := {
//     case PathList("META-INF", xs @ _*) => MergeStrategy.discard
//     case "application.conf"            => MergeStrategy.concat
//     case x =>
//       val oldStrategy = (assemblyMergeStrategy in assembly).value
//       oldStrategy(x)
//   }
// )

// COMMANDS
///////////////////////////////////////////////////////////////////////////////

// spark-submit --master spark://hadoop:7077 --class "SimpleApp" ./sparkWordCount/target/scala-2.11/sparkwordcount_2.11-0.1.0-SNAPSHOT.jar
def helloSbt = Command.command("hello") { state =>
  println("Hello, SBT")
  state
}

def sparkPreSubmitWordCountSbt = Command.command("sparkPreSubmitWordCount") { state =>
  Seq(
    "hdfs dfs -mkdir -p /app/samples" !,
    "hdfs dfs -put -f README.md /app/samples/" !
  )
  state
}

def sparkSubmitWordCountSbt = Command.command("sparkSubmitWordCount") { state =>
  Seq(
    "spark-submit --master spark://hadoop:7077 --class SimpleApp sparkWordCount/target/scala-2.11/simple-project_2.11-1.0.jar" !
  )
  state
}

commands += helloSbt
commands += sparkPreSubmitWordCountSbt
commands += sparkSubmitWordCountSbt
commands += Command.command("sparkSubmit") { state =>
  "project sparkWordCount" ::
  "package" ::
  "project global" ::
  "sparkPreSubmitWordCount" ::
  "sparkSubmitWordCount" ::
  state
}
