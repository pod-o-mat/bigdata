import $ivy.`com.twitter::finagle-http:6.36.0`
import $ivy.`io.github.hbase4s::hbase4s-core:0.1.2`

// HTTP Server
import com.twitter.finagle._, com.twitter.util._
import scala.util.{Success, Failure}

// HBase
import io.github.hbase4s._
import io.github.hbase4s.filter._
import io.github.hbase4s.config.HBasePropsConfig
import io.github.hbase4s.utils.HBaseImplicitUtils._


// --- Description of a CloudEvents
case class CloudEvent[M, D](metadata: M, data: D)
case class Ce(id: String, eventType: String, source: String, timestamp: String, subject: String, datacontenttype: String)
case class Evt(message: String)

val service = new Service[http.Request, http.Response] {
	// --- Connect to HBase
	val table = "events" // with column families: ce for cloudEvents and evt for event
	val conf = new HBasePropsConfig(Map("hbase.zookeeper.quorum" -> "zookeeper.zookeeper.svc.cluster.local"))
	val client = new HBaseClient(new HBaseConnection(conf), table)

	def put(event: CloudEvent[Ce, Evt]) = {
		// Write the Metadata
		client.put(
			s"${event.metadata.subject}:${event.metadata.eventType}:${event.metadata.timestamp}:${event.metadata.id}",
			event.metadata
		)
		// Write the Data
		client.put(
			s"${event.metadata.subject}:${event.metadata.eventType}:${event.metadata.timestamp}:${event.metadata.id}",
			event.data
		)	
	}

	def scan(key: String) : List[Evt] = {
		println(s"scan ${key}")
		client.scan[String](
			s"""row_prefix == "$key""""
		).map(_.typed[Evt].asClass).toList
	}

  	def apply(request: http.Request): Future[http.Response] = {
		request.method match {
	        case http.Method.Post =>
    		    request.uri match {
            		case "/" =>
			            println("POST /")
			            val str = request.getContentString()
			            val response = http.Response(http.Version.Http11, http.Status.Ok)
			            response.contentString = "Hello..!! " + str
			            Future.value(response)

		            case "/event" =>
			            println("POST /event")
			            val message = request.getContentString()
			            Future {
							val event = CloudEvent[Ce, Evt](
								Ce(
									"74F350D3-D9DD-415C-A138-DD2FCFC9198B",
									"com.ruggedcode.events.hbase.put",
									"com.ruggedcode.ammonite.hbase.tutorial",
									"2019-11-02 15:11:26 EDT",
									"00000001",
									"text/plain"
								),
								Evt(message)
							)
							put(event)
				            val response = http.Response(http.Version.Http11, http.Status.Ok)
				            response.contentString = "OK: " + message
				            response
			            }
		            case _ =>
			            println("POST /404")
			            Future.value(http.Response(http.Version.Http11, http.Status.NotFound))
        		}
        	case http.Method.Get =>
        		request.uri match {
            		case "/" =>
		            	println("GET /")
			            val str = request.getContentString()
			            val response = http.Response(http.Version.Http11, http.Status.Ok)
			            response.contentString = "Thank You " + str
			            Future.value(response)
		            case "/event" =>
		            	println("GET /event")
						Future {
							println("future")
				            val key = request.getContentString()
					        val response = http.Response(http.Version.Http11, http.Status.Ok)
					        println("scanning")
							val events : List[Evt] = scan(key)
							val body = events.map(_.message).mkString("\n")
							println(s"Body = ${body}")
							response.contentString = body
							response
						}
		            case _ =>
    			        println("GET /404")
			            Future.value(http.Response(http.Version.Http11, http.Status.NotFound))
        		}
		}

	}
}

println("listening to 8080")
val server = Http.serve(":8080", service)
Await.ready(server)
